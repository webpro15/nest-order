import { IsNotEmpty } from 'class-validator';

export class CreateOrderDto {
  customerId: number;
  orderItems: CreatedOrderItemDto[];
}

class CreatedOrderItemDto {
  @IsNotEmpty()
  productId: number;
  @IsNotEmpty()
  amount: number;
}
